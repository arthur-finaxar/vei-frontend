# VEI Frontend

## Run local development server

`yarn start`

## Run tests

`yarn test`

## Format code with prettier

`yarn run format`

## Run storybook server

`yarn run storybook`
